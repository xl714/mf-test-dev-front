var scale = 1;
$( document ).scroll(function() {
    var scrollTop = $(document).scrollTop();
    var bottom = $(window).scrollTop() + $(window).height() == $(document).height();
    if(!bottom){
        var multi = scrollTop * 2;
        if(scrollTop >= 0){
        	if(multi <= 40){
                scale = 1 + scrollTop/10;
                $("#intro").css({
                    "transform": "rotateX("+multi+"deg) scale("+scale+")"
                });
            }else if(multi <= 5000){
              	$("#intro").css({
                    "transform": "rotateX(70deg) translateZ("+-((scrollTop-30)/1000)+"rem) translateY(-"+((scrollTop - 20))+"rem) scale("+scale+")",
                    "perspective-origin": "center -400px;"
                });
            }
        }
    }
});

$(document).scroll(function() {
    var scrollTop = $(document).scrollTop();
    var bottom = scrollTop + $(window).height() == $(document).height();
    if (bottom) return;

    var
        sidebarJqEl = $("#top-right-side-bar"),
        sidebarHeight = sidebarJqEl.outerHeight(),
        sidebarPosition = sidebarJqEl.position(),
        sidebarTop = sidebarPosition.top,
        sidebarBottom = sidebarTop + sidebarHeight,
        leftContentJqEl = $("#top-right-side-bar").prev(),
        leftContentHeight = leftContentJqEl.height(),
        leftContentPosition = leftContentJqEl.position(),
        leftContentTop = leftContentPosition.top,
        leftContentBottom = leftContentTop + leftContentHeight;

    var
        sidebarNewTop = scrollTop - leftContentTop + 50,
        sidebarNewBottom = sidebarNewTop + sidebarHeight + sidebarJqEl.css("marginTop").replace('px', '') * 1 + +sidebarJqEl.css("marginBottom").replace('px', '') * 1;

    if (sidebarNewTop >= 0 && sidebarNewBottom <= leftContentHeight && scrollTop >= leftContentTop) {
        sidebarJqEl.css({
            top: sidebarNewTop + 'px'
        });
    }
});
