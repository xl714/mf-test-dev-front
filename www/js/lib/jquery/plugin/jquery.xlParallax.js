var _xlParallax = [];

$.fn.xlParallax = function( options ) {

  var settings = $.extend({
    speed: 10
  }, options );


  return this.each(function() {
    if(! $(this).attr("data-xlparallax-id")){

      var id = _xlParallax.length +1;
      var height = $(this).outerHeight();
      //console.log('height', height);

      $(this)
        .attr("data-xlparallax-id", id)
        .attr("data-xlparallax-height", height)
        .css({"display":"block", "overflow" : "hidden"});

      _xlParallax.push($(this));

    }
  }); // end of return this.each(
};

$( document ).scroll(function() {
  var scrollTop = $(document).scrollTop();
  // console.log('scrollTop', scrollTop);
  var bottom = $(window).scrollTop() + $(window).height() == $(document).height();
  if(_xlParallax.length && !bottom){
     for(var i in _xlParallax){
       var jqEl = _xlParallax[i];

       var height = jqEl.attr('data-xlparallax-height')*1;
       var missScroll = jqEl.attr('data-xlparallax-miss-scroll')*1 || 0;
       // console.log('missScroll', missScroll);
       // est-ce que le bas de l'element est visible ?

       var rect = jqEl.get(0).getBoundingClientRect();
       var isOffScreen = (rect.y + rect.height) < 0 || ( rect.y > window.innerHeight);

  		 var rectNext = jqEl.next().get(0).getBoundingClientRect();
       var nextIsOffScreen = (rectNext.y + rectNext.height) < 0 || ( rectNext.y > window.innerHeight);

       // console.log(height + ' - ' + 1.5*scrollTop + ' + ' + missScroll + ' + ' + (height - scrollTop + missScroll));

       if(!nextIsOffScreen)
       {
           calc = height - scrollTop + missScroll;
           newHeight = (calc > height) ? height : calc;
         jqEl.css({ 'height': newHeight+"px" });
       }
       else
       {
         jqEl.attr("data-xlparallax-miss-scroll", scrollTop)
       }
     }
   }
});


$(".xl-parallax").xlParallax({speed:15})
